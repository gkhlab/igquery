from django.db import models
from django.core.validators import MinLengthValidator, MaxLengthValidator
from simple_history.models import HistoricalRecords

class Species(models.Model):
    name = models.CharField(max_length=200, unique=True)
    aux = models.CharField(max_length=200, blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.name

class Locus(models.Model):
    name = models.CharField(max_length=200)
    species = models.ForeignKey(Species, on_delete=models.CASCADE)
    history = HistoricalRecords()

    def __str__(self):
        return self.name

class Chain(models.Model):
    name = models.CharField(max_length=200)
    species = models.ForeignKey(Species, on_delete=models.CASCADE)
    locus = models.ForeignKey(Locus, on_delete=models.CASCADE)
    history = HistoricalRecords()

    def __str__(self):
        return self.name

class Geneset(models.Model):
    type = models.CharField(choices=[
        ('V','V gene'),
        ('D','D gene'),
        ('J','J gene'),
        ],
        max_length=1,
        default='V'
    )
    history = HistoricalRecords()

    def __str__(self):
        return self.type

class Gene(models.Model):
    name = models.CharField(max_length=200)
    species = models.ForeignKey(Species, on_delete=models.CASCADE)
    chain = models.ForeignKey(Chain, on_delete=models.CASCADE)
    locus = models.ForeignKey(Locus, on_delete=models.CASCADE)
    geneset = models.ForeignKey(Geneset, on_delete=models.CASCADE)
    history = HistoricalRecords()

    def __str__(self):
        return self.name

class Allele(models.Model):
    name = models.CharField(max_length=200)
    species = models.ForeignKey(Species, on_delete=models.CASCADE)
    chain = models.ForeignKey(Chain, on_delete=models.CASCADE)
    locus = models.ForeignKey(Locus, on_delete=models.CASCADE)
    geneset = models.ForeignKey(Geneset, on_delete=models.CASCADE)
    gene = models.ForeignKey(Gene, on_delete=models.CASCADE)
    history = HistoricalRecords()

    def __str__(self):
        return self.name

class Sequence(models.Model):
    species = models.ForeignKey(Species, on_delete=models.CASCADE)
    chain = models.ForeignKey(Chain, on_delete=models.CASCADE)
    locus = models.ForeignKey(Locus, on_delete=models.CASCADE)
    geneset = models.ForeignKey(Geneset, on_delete=models.CASCADE)
    gene = models.ForeignKey(Gene, on_delete=models.CASCADE)
    allele = models.ForeignKey(Allele, on_delete=models.CASCADE)
    sequence = models.TextField()
    source = models.CharField(choices=(
            ('g', "Genomic"),
            ('e', "Expressed"),
        ),
        max_length=1
    )
    history = HistoricalRecords()

    def __str__(self):
        return self.allele.name
    pub_date = models.DateTimeField('date published')

class LogEntry(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    pub_date = models.DateTimeField('date published')
    history = HistoricalRecords()

    def __str__(self):
        return self.title

class Version(models.Model):
    tag = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published', auto_now_add=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.tag

class FastaField(models.Model):
    fasta_text = models.TextField(validators=[MaxLengthValidator(5000), MinLengthValidator(3)])
    species_name = models.CharField(max_length=200, default='')
    locus_name = models.CharField(max_length=200, default='')
    chain_name = models.CharField(max_length=200, default='')
    upload_date = models.DateTimeField(auto_now_add=True, blank=True)
    def __str__(self):
        return str(self.upload_date)
