from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin

# Register your models here.
from .models import Species, Locus, Chain, Geneset, Gene, Allele, Sequence

from .models import LogEntry
from .models import FastaField
from .models import Version

admin.site.register(LogEntry, SimpleHistoryAdmin)
admin.site.register(FastaField, SimpleHistoryAdmin)
admin.site.register(Version, SimpleHistoryAdmin)

admin.site.register(Species, SimpleHistoryAdmin)
admin.site.register(Locus, SimpleHistoryAdmin)
admin.site.register(Chain, SimpleHistoryAdmin)
admin.site.register(Geneset, SimpleHistoryAdmin)
admin.site.register(Gene, SimpleHistoryAdmin)
admin.site.register(Allele, SimpleHistoryAdmin)
admin.site.register(Sequence,SimpleHistoryAdmin)
