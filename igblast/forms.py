from django import forms
from .models import FastaField

class ModelFormWithFastaField(forms.ModelForm):
    class Meta:
        model = FastaField
        fields = ('fasta_text','species_name','locus_name','chain_name')
