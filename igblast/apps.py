from django.apps import AppConfig


class IgblastConfig(AppConfig):
    name = 'igblast'
