from django.urls import path
from . import views

urlpatterns = [
    path('', views.home , name='home'),
    path('datasets/<species>/<locus>/<chain>/<geneset>/', views.send_file, name='send_file'),
    path('datasets/', views.datasets, name='datasets'),
    path('search/', views.search, name='search'),
    path('result/', views.result, name='result'),
    path('about/', views.about, name='about'),

]
