from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.http import HttpResponseRedirect

from .models import Locus
from .models import Chain
from .models import Gene
from .models import Geneset
from .models import Species
from .models import Allele
from .models import Sequence

from .models import LogEntry
from .models import FastaField
from .models import Version

from .tasks import process_fasta
from .forms import ModelFormWithFastaField

from django.http import FileResponse
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage

def create_db(species, locus, chain, geneset):
    """
    Create file with specific database
    """
    sp = Species.objects.all()
    if species != 'All':
        sp = [Species.objects.filter(name=species).first()]
    seq_dict = {}
    version = Version.objects.order_by('-id').first() # Overall database version, no need to be in loop
    gset = Geneset.objects.filter(type=geneset).first()
    for s in sp:
        loc = Locus.objects.filter(species=s, name=locus).first()
        ch = Chain.objects.filter(species=s, locus=loc, name=chain).first()
        allele_all = Allele.objects.filter(species=s, locus=loc, chain=ch, geneset=gset).all()
        for allele in allele_all:
            sequence = Sequence.objects.filter(species=s, locus=loc, chain=ch, geneset=gset, allele=allele).first()
            seq_dict[str(allele)] = str(sequence.sequence)

    path = species.replace(' ','-')+'_'+locus+'_'+chain+'_'+geneset+'_'+str(version).replace('.','-')+'.fasta'
    fasta = default_storage.open(path, 'wt')
    for allele_name,allele_seq in seq_dict.items():
        fasta.write(">{}\n{}\n".format(allele_name,allele_seq))
    if len(seq_dict) == 0:
        fasta.write('>dummy\nNNNNNN\n')
    fasta.close()
    return default_storage.path(path)

def send_file(response, species, locus, chain, geneset):
    path = create_db(species, locus, chain, geneset)
    fasta = open(path, 'rb')
    response = FileResponse(fasta)
    return response

def datasets(request):
    """Database overview"""
    context = {}
    for species in Species.objects.all():
        context.setdefault(str(species),{})
        for locus in Locus.objects.filter(species=species):
            context[str(species)].setdefault(str(locus),{})
            for chain in Chain.objects.filter(locus=locus):
                context[str(species)][str(locus)].setdefault(str(chain),{})
                for geneset in Geneset.objects.all():
                    context[str(species)][str(locus)][str(chain)].setdefault(str(geneset),[])
                    for gene in Gene.objects.filter(locus=locus,chain=chain, geneset=geneset):
                        for allele in Allele.objects.filter(gene=gene):
                            context[str(species)][str(locus)][str(chain)][str(geneset)].append(str(allele))

    return render(request, 'igblast/datasets.html', {'context': context})

def home(request):
    latest_changelog_list = LogEntry.objects.order_by('-pub_date')[:5]
    version = Version.objects.order_by('-id').first()
    context = {'latest_changelog_list': latest_changelog_list, 'version': version}
    return render(request, 'igblast/home.html', context)

def search(request):
    species = Species.objects.all()
    locus = set([l.name for l in Locus.objects.distinct()])
    chain = set([l.name for l in Chain.objects.distinct()])
    version = Version.objects.order_by('-id').first()
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ModelFormWithFastaField(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            path = default_storage.save('query.fa', ContentFile(form.cleaned_data['fasta_text']))
            request.session['fasta_file'] = default_storage.path(path)
            if form.cleaned_data['species_name'] != 'All':
                sp = Species.objects.filter(name=form.cleaned_data['species_name']).first()
                request.session['aux'] = sp.aux
            else:
                request.session['aux'] = 'database/rhesus_monkey_gl.aux' # Default aux
            request.session['species'] = form.cleaned_data['species_name']
            # create db
            database = {}
            for geneset in ['V','D','J']:
                database[geneset] = create_db(form.cleaned_data['species_name'],form.cleaned_data['locus_name'], form.cleaned_data['chain_name'], geneset)
            request.session['database'] = database
            # save data in database
            form.save()
            # redirect to a new URL:
            return HttpResponseRedirect('/result/')
    # if a GET (or any other method) we'll create a blank form
    else:
        form = ModelFormWithFastaField()

    return render(request, 'igblast/search.html', {'form':form, 'species':species, 'locus':locus, 'chain':chain, 'version':version})

def result(request):
    if request.session.has_key('fasta_file'):
        fasta_file = request.session.get('fasta_file')
        database = request.session.get('database')
        aux = request.session.get('aux')
        species = request.session.get('species')
        output_file = process_fasta.delay(fasta_file, database, aux)
        return render(request, 'igblast/result.html', {'fasta_text':fasta_file[0:10]+'...', 'table': output_file.get(), 'database':database, 'aux':aux, 'species':species})
    return render(request, 'igblast/result.html')

def about(request):
    return render(request, 'igblast/about.html')
