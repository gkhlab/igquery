from celery import shared_task
from celery.decorators import task
from celery.utils.log import get_task_logger
from django.conf import settings
from io import StringIO
import pandas as pd

import subprocess
import os

logger = get_task_logger(__name__)

@shared_task
def sample_task(name='sample_task'):
    print("The sample task just ran.")

def run_process(cmd_args):
    with subprocess.Popen(cmd_args, stdout=subprocess.PIPE) as proc:
        output = proc.stdout.read()
        logger.info("Executing: {}".format(' '.join(cmd_args)))
        return output.decode('utf-8')

@task(name="process_fasta")
def process_fasta(query_file, database, aux):
    logger.info("Processing {}".format(query_file))

    output = query_file.replace('fasta','tsv')

    # Index database if needed
    if not os.path.isfile(database['V']+'.ndb'):
        run_process(['/ncbi-igblast/bin/makeblastdb','-parse_seqids','-dbtype','nucl','-in', database['V']])
    if not os.path.isfile(database['D']+'.ndb'):
        run_process(['/ncbi-igblast/bin/makeblastdb','-parse_seqids','-dbtype','nucl','-in', database['D']])
    if not os.path.isfile(database['J']+'.ndb'):
        run_process(['/ncbi-igblast/bin/makeblastdb','-parse_seqids','-dbtype','nucl','-in', database['J']])

    # Run IgBLAST
    stdout = run_process(['/ncbi-igblast/bin/igblastn', '-num_threads', '4',
                 '-auxiliary_data', aux,
                 '-organism','rhesus_monkey',
                 '-ig_seqtype','Ig',
                 '-num_alignments_V','1',
                 '-num_alignments_D','1',
                 '-num_alignments_J','1',
                 '-germline_db_V',database['V'],
                 '-germline_db_D',database['D'],
                 '-germline_db_J',database['J'],
                 '-query', query_file,
                 '-outfmt','19',
                 '-out', '-'])
    try:
        df = pd.read_csv(StringIO(stdout),sep='\t')
    except pd.errors.EmptyDataError:
        return "<p>No matches</p>"

    for region in ['fwr1','fwr2','fwr3','fwr4','cdr1','cdr2','cdr3']:
        df[region+'_start'] = df[region+'_start'].fillna(0).astype(int)
        df[region+'_end'] = df[region+'_end'].fillna(0).astype(int)
        df.loc[:,region+'_nt'] = df.apply(lambda x: x['sequence'][slice(x[region+'_start'],x[region+'_end'])],axis=1)
    html = df.to_html(table_id='tsv',classes=['table table-striped table-bordered'])
    return html
