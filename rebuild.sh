docker-compose down
docker-compose up --build -d
docker-compose run web /usr/local/bin/python manage.py collectstatic --noinput
docker-compose run web /usr/local/bin/python manage.py makemigrations igblast
docker-compose run web /usr/local/bin/python manage.py migrate

