# Website for hosting NGS inferred sequences

Requirements
- docker.io >= 20.10
- docker-compose >= 1.27.4
- docker-machine >= 0.16.0 on non GNU/Linux machines (i.e Windows)

## Installation

First step is to start containers
```
# docker-compose down # Uncomment this if you want to rebuild code
docker-compose up --build -d
docker-compose run web /usr/local/bin/python manage.py collectstatic --noinput
docker-compose run web /usr/local/bin/python manage.py makemigrations igblast
docker-compose run web /usr/local/bin/python manage.py migrate
```
Secondly, create super user required for editing website from *admin* interface
```
docker-compose run web /usr/local/bin/python manage.py createsuperuser
```

Third create in the root folder of this repository a new folder **database** with V.fasta, D.fasta and J.fasta sequences.

Finally, download **IgBLAST** program both in the droplet where website will be deployed and locally
```
wget https://ftp.ncbi.nih.gov/blast/executables/igblast/release/LATEST/ncbi-igblast-1.18.0-x64-linux.tar.gz
tar xvfz ncbi-igblast-1.18.0-x64-linux.tar.gz
ln -s ncbi-igblast-1.18.0/internal_data/
```

## Populating database

First place files in TSV format into *database* folder which gets copied into docker image during installation step. Following mandatory columns are required
- allele_name
- gene_name
- locus_name
- species_name
- allele_sequence

Start web container app django shell
```
docker-compose run web python manage.py shell  
```
and run the following script that populates SQL database with content from TSV files
```
from igblast.models import Chain
from igblast.models import Locus
from igblast.models import Gene
from igblast.models import Geneset
from igblast.models import Species
from igblast.models import Allele
from igblast.models import Sequence

import pandas as pd
import django
from django.utils import timezone
django.setup()

table = pd.read_csv('database/J.tsv',sep='\t')
for _,row in table.iterrows():
    species,_=  Species.objects.get_or_create(name=row['species_name'])
    locus,_ = Locus.objects.get_or_create(species=species, name=row['locus_name'])
    chain,_ = Chain.objects.get_or_create(species=species, locus=locus, name='Heavy')
    geneset,_ = Geneset.objects.get_or_create(type='J')
    gene,_ = Gene.objects.get_or_create(species=species, locus=locus, chain=chain, geneset=geneset, name=row['gene_name'])
    allele,_ = Allele.objects.get_or_create(species=species, locus=locus, chain=chain, geneset=geneset, gene=gene, name=row['allele_name'])
    sequence,_ = Sequence.objects.get_or_create(species=species, locus=locus, chain=chain, geneset=geneset, gene=gene,  allele=allele, sequence=row['allele_sequence'], source='g', pub_date=timezone.now())
```
Repeat this step for Vs, Ds or remaining data.

## Updating database
First step is to deploy database locally and test changes by following steps described above.

To update database

1. Login via admin interface http://localhost/admin for which password can be set with standard manage script

```
docker-compose run web python manage.py changepassword
```

2. Navigate to Versions and click '+Add' then type in new version tag which should be number in format major.minor (i.e 1.1, 1.2, 1.3,...2.0,2.1..etc).

Modify database content using GUI or use custom SQL scripts and manage command.

3. Refresh website http://localhost to see changes

4. (Optional) Git commit necessary changes and repeat deployment to test changes in templates

```
docker-compose down # Uncomment this if you want to rebuild code
docker-compose up --build -d
docker-compose run web /usr/local/bin/python manage.py collectstatic --noinput
docker-compose run web /usr/local/bin/python manage.py makemigrations igblast
docker-compose run web /usr/local/bin/python manage.py migrate
```

5. Git push, ssh to droplet kimdb.gkhlab.se (188.166.48.97) and repeat step **4.** to deploy changes.
Database remains unchanged.

**NOTE**: In an unlikely event that needs resetting whole database check Django documentation for **flush** command.
Normal update do NOT require flushing, and further updates should be carried out through admin interface or with custom scripts.
