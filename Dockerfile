FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code
RUN pip install -r requirements.txt
RUN apt update && apt install -y libuv1-dev postgresql-client
COPY ./ /code
# expose the port 8000
EXPOSE 8000
# define the default command to run when starting the container
CMD ["gunicorn", "--bind", ":8000", "igquery.wsgi:application"]
